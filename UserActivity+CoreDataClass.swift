//
//  UserActivity+CoreDataClass.swift
//  
//
//  Created by iosdev on 24.4.2021.
//
//

import Foundation
import CoreData


public class UserActivity: NSManagedObject {

    /**
     When calling this function, all the UserActivityfields need to be initialized.
     */
    class func createUserActivityManagedObjectContext (_ temp: TempUserActivity) {
        
        let req: NSFetchRequest<UserActivity> = UserActivity.fetchRequest()
        req.predicate = NSPredicate(format: "id = %@", temp.id as CVarArg)
        
        let context = AppDelegate.viewContext
        
        if let idMatches = try? context.fetch(req) {
            if (idMatches.count == 0) {
                let newActivity = UserActivity(context: context)
                newActivity.id = temp.id
                newActivity.locationGeopoints = temp.locationGeopoints as NSObject
                newActivity.distanceTravelled = temp.distanceTravelled
                newActivity.caloriesBurned = temp.caloriesBurned ?? 0
                newActivity.timeStamp = temp.timeStamp
                newActivity.averageSpeed = temp.averageSpeed ?? 0
                newActivity.totaltime = temp.totaltime ?? 0
            }
        }
    }
    
}
