//
//  Medal+CoreDataProperties.swift
//  SportsTracker
//
//  Created by iosdev on 6.5.2021.
//  Copyright © 2021 iosdev. All rights reserved.
//
//

import Foundation
import CoreData


extension Medal {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Medal> {
        return NSFetchRequest<Medal>(entityName: "Medal")
    }

    @NSManaged public var activityId: UUID?
    @NSManaged public var desc: String?
    @NSManaged public var id: UUID?
    @NSManaged public var identifier: String?
    @NSManaged public var imageFile: String?
    @NSManaged public var timeStamp: Date?

}

extension Medal : Identifiable {

}
