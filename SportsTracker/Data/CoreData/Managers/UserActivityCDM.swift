//
//  UserActivityCDM.swift
//  SportsTracker
//
//  Created by iosdev on 24.4.2021.
//  Copyright © 2021 iosdev. All rights reserved.
//

import Foundation
import CoreData

/**
 Handles the UserActivity related communication with CoreData.
  */
class UserActivityCDM: CoreDataManager {
        
    // Instance of the class
    static var sharedInstance: CoreDataManager = {
        let instance = UserActivityCDM()
        return instance
    }()
    
    private let managedObjectContext = AppDelegate.viewContext
    
    
    /**
     Returns the last saved UserActivity from CoreData.
     */
    func requestLatestUserActivity() -> UserActivity? {
        let request: NSFetchRequest = UserActivity.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "timeStamp", ascending: false)]
        request.fetchLimit = 1
        
        var activity: UserActivity?
        try? activity = managedObjectContext.fetch(request)[0]
        return activity
    }
    
    /**
     Returns ALL the UserActivities saved into CoreData in descending order.
     */
    func requestAllUserActivities() -> [UserActivity] {
        let request: NSFetchRequest = UserActivity.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "timeStamp", ascending: false)]
        
        var results = [UserActivity]()
        try? results = managedObjectContext.fetch(request)
        return results
    }
    
    /**
     Delete an activity from CoreData
     */
    func deleteActivity(_ activity: UserActivity) {
        managedObjectContext.delete(activity)
        
        // Also, remove the medals
        let request: NSFetchRequest = Medal.fetchRequest()
        let query = NSPredicate(format: "activityId == %@", activity.id! as CVarArg)
        request.predicate = query
        do {
            let results = try managedObjectContext.fetch(request)
            results.forEach { it in
                managedObjectContext.delete(it)
            }
            try managedObjectContext.save()
        } catch let err {
            print("Error deleting stuff: \(err)")
            // handle the Core Data error
        }
    }
    
    
    /**
     Call to save an object to CoreData.
     Parameter: forObject: TempUserActivity or MedalObjects
     */
    func requestSaveAction<T>(forObject: T) -> CDMRequestResponse {
        
        if forObject is TempUserActivity {
            return saveUserActivity(forObject as! TempUserActivity)
        }
        
        if forObject is MedalObject {
            return saveMedal(forObject as! MedalObject)
        }
        
        if forObject is [MedalObject] {
            (forObject as! [MedalObject] ).forEach { it in
                let res = saveMedal(it)
                if res == .failure {
                    print("Failed to save a single medal")
                }
            }
            return .success
        }
        
        return .invalidRequest
    }
    
    /**
     Save the activity.
     */
    private func saveUserActivity(_ temp: TempUserActivity) -> CDMRequestResponse {
        do {
            UserActivity.createUserActivityManagedObjectContext(temp)
            try self.managedObjectContext.save()
            return .success
        } catch let err {
            print("Error saving UserActivity to coredata: \(err)")
            return .failure
        }
    }
    
    private func saveMedal(_ medal: MedalObject) -> CDMRequestResponse {
        do {
            Medal.createMedalManagedObjectContext(medal)
            try self.managedObjectContext.save()
            return .success
        } catch let err {
            print("Error saving medal : \(err)")
            return .failure
        }
    }
    
    
}
