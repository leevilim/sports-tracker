//
//  MedalCDM.swift
//  SportsTracker
//
//  Created by leevilim on 5.5.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation
import CoreData

/**
 Handles the Medal related communication with CoreData.
 */
class MedalCDM: CoreDataManager {
    
    // Instance of the class
    static var sharedInstance: CoreDataManager = {
        let instance = MedalCDM()
        return instance
    }()
    
    private let managedObjectContext = AppDelegate.viewContext
    
    
    /**
     Get medals for specific UserActivity
     */
    func getMedalsForActivity(_ activity: UserActivity) -> [Medal] {
        let request: NSFetchRequest = Medal.fetchRequest()
        
        let query = NSPredicate(format: "activityId == %@", activity.id! as CVarArg)
        request.predicate = query
        
        print("Fetching medals")
        // Perform the fetch with the predicate
        do {
            let results = try managedObjectContext.fetch(request)
            return results
        } catch let err {
            print("Error fetching medals from CoreData: \(err)")
        }
        
        return [Medal]()
    }
    
    /**
     Return ALL medals from CoreData
     */
    func getAllMedals() -> [Medal] {
        let request: NSFetchRequest = Medal.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        
        var results = [Medal]()
        try? results = managedObjectContext.fetch(request)
        return results
    }
    
    /**
     Return medals via identifier. Identifiers are defined in each of the classes implementing MedalObject. @see Medals.swift
     */
    func getMedalsViaIdentifier(string: String) -> [Medal] {
        let request: NSFetchRequest = Medal.fetchRequest()
        
        let query = NSPredicate(format: "identifier == %@", string as CVarArg)
        request.predicate = query
        
        do {
            let results = try managedObjectContext.fetch(request)
            return results
        } catch let err {
            print("Error fetching medals from CoreData: \(err)")
        }
        
        return [Medal]()
    }
    
    /**
     Get medals up to n count, in time order
     */
    func getMostRecentMedals(count: Int) -> [Medal] {
        let request: NSFetchRequest = Medal.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "timeStamp", ascending: false)]
        request.fetchLimit = count
        
        var res = [Medal]()
        try? res.append(contentsOf: managedObjectContext.fetch(request))
        return res
    }
    
    
    /**
     Get medals of certain type in a timeframe from provided day to today
     */
    func getMedalsUpFromDate(date: Date, identifier: String) -> [Medal] {
        let request: NSFetchRequest = Medal.fetchRequest()
        let query = NSPredicate(format: "timeStamp => %@ AND identifier == %@", date as CVarArg, identifier as CVarArg)
        request.predicate = query
        
        var res = [Medal]()
        try? res.append(contentsOf: managedObjectContext.fetch(request))
        print("MedalCount from Compound query: \(res.count)")
        return res
    }
    
    
    
    /**
     Save MedalObjects
     */
    func requestSaveAction<T>(forObject: T) -> CDMRequestResponse {
        
        if forObject is MedalObject {
            return saveMedal(forObject as! MedalObject)
        }
        
        if forObject is [MedalObject] {
            (forObject as! [MedalObject] ).forEach { it in
                let res = saveMedal(it)
                if res == .failure {
                    print("Failed to save a single medal")
                }
            }
            return .success
        }
        
        return .invalidRequest
    }
    
    
    private func saveMedal(_ medal: MedalObject) -> CDMRequestResponse {
        do {
            Medal.createMedalManagedObjectContext(medal)
            try self.managedObjectContext.save()
            return .success
        } catch let err {
            print("Error saving medal : \(err)")
            return .failure
        }
    }
    
}
