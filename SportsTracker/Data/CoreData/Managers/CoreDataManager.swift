//
//  CoreDataManager.swift
//  SportsTracker
//
//  Created by leevilim on 24.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation
import CoreData

/**
 Defines the methods every CoreDataManager class in the application needs to have in addition to their own functionalities.
 */
protocol CoreDataManager {

    /**
     Instance of the CoreDataManager.
     */
    static var sharedInstance: CoreDataManager { get }
        
    /**
     Request to save an object to CoreData.
     Returns CDMRequestResponse based on the outcome of function.
     */
    func requestSaveAction <T> (forObject: T) -> CDMRequestResponse
    
}

/**
 Different responses a CoreDataManager protocol method can give.
 */
enum CDMRequestResponse {
    case success; // Request completed successfully
    case invalidRequest; // Invalid parameters for the fuction
    case failure; // Request failed for other reasons
}
