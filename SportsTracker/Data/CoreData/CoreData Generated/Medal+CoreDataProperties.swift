//
//  Medal+CoreDataProperties.swift
//  
//
//  Created by iosdev on 5.5.2021.
//
//

import Foundation
import CoreData


extension Medal {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Medal> {
        return NSFetchRequest<Medal>(entityName: "Medal")
    }

    @NSManaged public var activityId: UUID?
    @NSManaged public var desc: String?
    @NSManaged public var identifier: String?
    @NSManaged public var imageFile: String?
    @NSManaged public var id: UUID?
}
