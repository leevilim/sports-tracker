//
//  UserActivity+CoreDataProperties.swift
//  
//
//  Created by iosdev on 26.4.2021.
//
//

import Foundation
import CoreData


extension UserActivity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserActivity> {
        return NSFetchRequest<UserActivity>(entityName: "UserActivity")
    }

    @NSManaged public var caloriesBurned: Double
    @NSManaged public var distanceTravelled: Double
    @NSManaged public var id: UUID?
    @NSManaged public var locationGeopoints: NSObject?
    @NSManaged public var timeStamp: Date?
    @NSManaged public var totaltime: Double
    @NSManaged public var averageSpeed: Double

}
