//
//  Medal+CoreDataClass.swift
//  
//
//  Created by iosdev on 5.5.2021.
//
//

import Foundation
import CoreData


public class Medal: NSManagedObject {

    /**
     When calling this function, all the UserActivityfields need to be initialized.
     */
    class func createMedalManagedObjectContext (_ medal: MedalObject) {
        
        let req: NSFetchRequest<Medal> = Medal.fetchRequest()
        req.predicate = NSPredicate(format: "id = %@", medal.id as CVarArg)
        
        let context = AppDelegate.viewContext
        
        if let idMatches = try? context.fetch(req) {
            if (idMatches.count == 0) {
                let newMedal = Medal(context: context)
                newMedal.id = medal.id
                newMedal.activityId = medal.activityId
                newMedal.desc = medal.description
                newMedal.identifier = medal.identifier
                newMedal.imageFile = medal.imageFile
            }
        }
    }
}
