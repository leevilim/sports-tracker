//
//  GroupedTableViewProvider.swift
//  SportsTracker
//
//  Created by leevilim on 26.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation

/**
 Protocol that divides given data into groups, so it can be easily used by TableViewControllers.
 
 Classes implementing need to have their own logic on how the items are grouped. This needs to be done in
 the initializer of the class.
 */
protocol GroupedTableViewProvider {
    
    /**
     Type T == Items we provide to the initializer, and which are displayed in TableView as Items
     Type U == How are the items grouped
     */
    associatedtype T;
    associatedtype U;

    /**
     Provide the array of items we want to display
     */
    init(initialDataSet: [T])
    
    /**
     Indexes of these arrays need to match, so that group[n] items can be found from items[n]
     */
    var groups: [U] { get }
    var items: [[T]] { get }
    
    
    func getGroupCount() -> Int
    
    func getItemCountForGroup(_ index: Int) -> Int
    
    func getGroupAtIndex(_ index: Int) -> U
    
    func getItemsAtGroupIndex(_ index: Int) -> [T]
    
    func getItemDataAtGroupIndex(groupIndex: Int, itemIndex: Int) -> T
    
}
