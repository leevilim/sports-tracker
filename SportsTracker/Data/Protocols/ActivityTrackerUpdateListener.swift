//
//  ActivityTrackerDelegate.swift
//  SportsTracker
//
//  Created by leevilim on 19.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation
import CoreLocation

/**
 Protocol which defines the required functionality for any class that wants updates from ActivityTracker
 */
protocol ActivityTrackerUpdateListener {
    func onActivityStateChanged(_ currentState: ActivityState)
    func onLocationChanged(_ latestLocation: CLLocation)
}

/**
 Different activity states controlled by the user actions.
 
 ActivityTracker calls the onActivityStateChanged methods with these enum values as parameters to indicate
 activity state changes.
 */
enum ActivityState {
    case notActive
    case running
    case paused
    case finished
}
