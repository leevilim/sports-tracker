//
//  UserDefaultManager.swift
//  SportsTracker
//
//  Created by leevilim on 4.5.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation

/**
 Provides the UserDefaults for the application
 */
class UserDefaultManager {
    
    // Instance of the class
    static var sharedInstance: UserDefaultManager = {
        let instance = UserDefaultManager()
        return instance
    }()
    
    private let defaults = UserDefaults.standard

    // Keys for defaults
    private let userName = "username"
    private let userWeight = "userweight"
    
    func getUserName() -> String {
        return defaults.string(forKey: userName) ?? ""
    }
    
    func getUserWeight() -> Double {
        return defaults.double(forKey: userWeight)
    }
    
    func setUserName(string: String) {
        defaults.set(string, forKey: userName)
    }
    
    func setUserWeight(weight: Double) {
        defaults.set(weight, forKey: userWeight)
    }
    
    /**
     Check if both the keys contain data in UserDefaults or not.
     
     Works as a check whether the application is started for the first time.
     */
    func hasData() -> Bool {
        return defaults.string(forKey: userName)?.isEmpty == false && defaults.double(forKey: userWeight) != 0.0
    }
}
