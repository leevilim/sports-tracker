//
//  ActivityGroupedProvider.swift
//  SportsTracker
//
//  Created by leevilim on 26.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation

/**
 Provides UserActivities in sectioned structure to easily display data in UITableViewController.
 */
class ActivityGroupedProvider: GroupedTableViewProvider {
    
    typealias T = UserActivity
    
    // Group UserActivites by month, as strings
    typealias U = String
    
    var groups: [U]
    var items: [[T]]
    
    
    required init(initialDataSet data: [T]) {
        groups = [U]()
        items = [[T]]()
        self.formGroups(data)
    }
    
    func getItemCountForGroup(_ index: Int) -> Int {
        if index == 0 && items.count == 0 {
            return 0 // Handle case where there is no data in history
        }
        return items[index].count
    }
    
    func getGroupCount() -> Int {
        return groups.count
    }
    
    func getGroupAtIndex(_ index: Int) -> String {
        if index == 0 && groups.count == 0 {
            return "" // Handle case where there is no data in history
        }
        return groups[index]
    }
    
    func getItemsAtGroupIndex(_ index: Int) -> [UserActivity] {
        return items[index]
    }
    
    func getItemDataAtGroupIndex(groupIndex: Int, itemIndex: Int) -> UserActivity {
        return items[groupIndex][itemIndex]
    }
    
    
    /**
     Creates groups and divides items between them based on months
     */
    private func formGroups(_ data: [UserActivity]) {
        // Activities grouped into months in a map
        let grouped = groupedByMonth(data)
       
        // Need to sort the months into array, map gives unreliable ordering, reversed to sort descending
        let sorted = grouped.sortedByKey.reversed()

        // Append all the data to class arrays
        sorted.forEach {
            let month = Dates.getMonthString(fromDate: $0.0)
            groups.append(month)
            items.append($0.1)
        }
        
    
    }
  

    /**
     Return activites divided into groups with months as keys
     */
    private func groupedByMonth(_ activities: [T]) -> [Date: [T]] {
        let empty: [Date: [T]] = [:]
        return activities.reduce(into: empty) { acc, cur in
            let components = Calendar.current.dateComponents([.year, .month], from: cur.timeStamp!)
            let date = Calendar.current.date(from: components)!
            let existing = acc[date] ?? []
            acc[date] = existing + [cur]
        }
    }

}
