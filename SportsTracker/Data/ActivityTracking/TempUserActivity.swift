//
//  TempUserActivity.swift
//  SportsTracker
//
//  Created by leevilim on 19.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation
import CoreLocation

/**
 Holds the data for a single user activity temporarily.
 
 This class is used to temporarily hold data while the users activity is running, and it is turned into a CoreData object when saving it.
 */
class TempUserActivity{

    
    let id: UUID = UUID()
    
    /**
     All the locations while the user was completing this activity are saved here.
     */
    private(set) var locationGeopoints = [CLLocation]()
    
    /**
     Total distance travelled in meters during this activity
     */
    private(set) var distanceTravelled: Double = 0
    
    /**
     When the activity was started
     */
    let timeStamp: Date = Date()
    
    private(set) var caloriesBurned: Double?
    
    // Time used in seconds for the activity
    private (set) var totaltime: Double?
    
    // (km/h)
    private (set) var averageSpeed: Double?

    
    /**
     Adds a location to the geopoints array, calculate/add the distance to total
     */
    func addLocation(_ loc: CLLocation) {
        // Add the distance to total
        if let last = locationGeopoints.last {
            distanceTravelled += loc.distance(from: last)
        }
        
        locationGeopoints.append(loc)
    }
    
    func getDistance() -> Double{
        return distanceTravelled
    }
    
    func setCalories(_ num: Double) {
        self.caloriesBurned = num
    }
    
    func setTotaltime(_ num: Double) {
        self.totaltime = num
    }
    
    func calculateSpeed() {
        guard let time = totaltime else {
            averageSpeed = 0
            return
        }
        averageSpeed = (distanceTravelled/time) * 3.6
    }
}
