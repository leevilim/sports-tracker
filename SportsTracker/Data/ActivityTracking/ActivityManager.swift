//
//  ActivityManager.swift
//  SportsTracker
//
//  Created by leevilim on 19.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation
import CoreLocation

/**
 A class that listens for updates in the ActivityTracker class.
 
 Handles the running activity's information and state based
 on the updates from ActivityTracker.
 */
class ActivityManager: ActivityTrackerUpdateListener, StopWatchDelegate {
    
    /**
     Register as a delegate to receive updates from this class.
     */
    var delegate: ActivityManagerUpdateListener?
    
    private let userActivity: TempUserActivity
    private let stopWatch: Stopwatch
    
    /**
     Pass in a CoreDataManager which is used to handle the CoreData communication for this class
     */
    private let coreDataManager: CoreDataManager
    private let defaults: UserDefaultManager
    
    /**
     MedalManager which defines if any medals were earned for finished activity
     */
    private let medalManager: MedalManager
    
    
    init(
        userActivity: TempUserActivity,
        stopWatch: Stopwatch,
        coreDataManager: CoreDataManager,
        defaultManager: UserDefaultManager,
        medalManager: MedalManager
    ) {
        self.userActivity = userActivity
        self.stopWatch = stopWatch
        self.coreDataManager = coreDataManager
        self.defaults = defaultManager
        self.medalManager = medalManager
        // Register self to get updates
        self.stopWatch.delegate = self
    }
    
    // MARK: ActivityTrackerUpdateListener methods
    
    func onActivityStateChanged(_ currentState: ActivityState) {
        print("Activity State: \(currentState)")
        switch currentState {
        case .running:
            start()
        case .paused:
            pause()
        case .finished:
            finish()
        default:
            print("Case is \(currentState)")
        }
    }
    
    
    func onLocationChanged(_ latestLocation: CLLocation) {
        
        // Since CoreLocation uses cached locations, if the location pushed in update is registered before the timeStamp of the running activity, ignore it.
        if latestLocation.shouldLocationBeIgnored(anotherDate: userActivity.timeStamp) {
            print("Skipped location")
            return
        }
        userActivity.addLocation(latestLocation)
        sendStatusUpdate(latestLocation, distanceInMeters: userActivity.getDistance())
    }
    
    // MARK: StopWatchDelegate
    
    // StopWatch value update
    func timerChanged(timerValue: Double) {
        print("Timer now : \(timerValue)")
        delegate?.onTimeChanged(timerValue)
    }
    
    // MARK: Private Methods
    
    private func start(){
        stopWatch.start()
    }
    
    private func pause(){
        stopWatch.stop()
    }
    
    private func finish(){
        stopWatch.stop()
         
        // Set the values for UserActivity before saving
        userActivity.setTotaltime(stopWatch.timerValue)
        userActivity.calculateSpeed()
        let calories = Calculator.burnedCalories(activity: userActivity, weight: defaults.getUserWeight())
        userActivity.setCalories(calories)
        
        // After all values are saved to activity, get medals for the activity if any, and save the activity itself along with all medals earned
        
        let medals = medalManager.getMedalForActivity(userActivity)
        
        let activityok = coreDataManager.requestSaveAction(forObject: userActivity)
        let medalsok = coreDataManager.requestSaveAction(forObject: medals)
        
        // Could handle errors here
        print("Saved activity / medals: \(activityok) \(medalsok)")
    }
    
    /**
     Calculate and forward the latest status update to delegate
     */
    private func sendStatusUpdate(_ fromLocation: CLLocation, distanceInMeters: Double){
        let speedAsKmh = Calculator.getAsKilometersPerHour(fromMetersPerSecond: fromLocation.speed)
        let distanceAsKm = Calculator.getAsKilometers(fromMeters: distanceInMeters)
        
        delegate?.onStatusUpdate(currentDistance: distanceAsKm, currentSpeed: speedAsKmh)
    }
}


/**
 Register to get updates from ActivityManager.
 */
protocol ActivityManagerUpdateListener {
    
    /**
     Called when there is an update to the timer used by ActivityManager
    */
    func onTimeChanged(_ timeValue: Double)
    
    /**
     Provides the most recent updates to distance travelled and the current speed the user is travelling at.
     */
    func onStatusUpdate(currentDistance: Double, currentSpeed: Double)
    
}
