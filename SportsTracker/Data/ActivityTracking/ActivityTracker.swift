//
//  ActivityTracker.swift
//  SportsTracker
//
//  Created by leevilim on 19.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation
import CoreLocation

/**
 Provides a tracker for the application, used when the user manually starts an activity.
 
 Collects location data for the time it is active. Forwards the activity status changes and location updates
 to delegate class implementing protocol ActivityTrackerUpdateListener.
 
 Implements the CLLocationManagerDelegate protocol to listen to location changes.
 */
class ActivityTracker: NSObject, CLLocationManagerDelegate {
    
    // MARK: Properties
    
    private let updateListener: ActivityTrackerUpdateListener
    private let locationManager: CLLocationManager
    private var isManagerSetup = false
    
    // MARK: Init
    
    /**
     Provide an instance of CLLocationManager in constructor.
     Also takes in an intance for a LocationChangeListener
     */
    init(manager: CLLocationManager, updateListener: ActivityTrackerUpdateListener) {
        self.locationManager = manager
        self.updateListener = updateListener
    }
    
    // MARK: Public methods
    
    /**
     Call when user starts the activity
     */
    func startActivity(){
        if(!isManagerSetup) {
            setUpLocationManager()
        }
        locationManager.startUpdatingLocation()
        updateListener.onActivityStateChanged(.running)
    }
    
    /**
     Call when user pauses the activity
     */
    func pauseActivity(){
        locationManager.stopUpdatingLocation()
        updateListener.onActivityStateChanged(.paused)
    }
    
    /**
     Call when user stops the activity
     */
    func finishActivity(){
        locationManager.stopUpdatingLocation()
        updateListener.onActivityStateChanged(.finished)
    }
    
    
    // MARK: Private Methods
    
    /**
     !! Needs to be called after initialization.
     Configs the LocationManager and sets self to be its delegate
     */
    private func setUpLocationManager(){
        // The distance in meters, how often we want updates
        locationManager.distanceFilter = 10
        locationManager.allowsBackgroundLocationUpdates = true
        
        // Set this class to be the delegate for the location manager
        locationManager.delegate = self
    }
    
    
    // MARK: LocationManager Delegate
    
    /**
     Handle the location updates here
     */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let myCoord = locations.last else {
            print("Location was null")
            return
        }
        updateListener.onLocationChanged(myCoord)
    }
    
    /**
     Any errors with the location service comes here
     */
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            // Location updates are not authorized.
            print("Authorization error in ActivityTracker")
            manager.stopUpdatingLocation()
            return
        }
        print("Other error in ActivityTracker : \(error)")
        // Notify the user of any errors.
    }
    
    /**
     Check if permission status has changed?
     */
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            // All good
            break
        default:
            // Request permissions again if something is wrong
            manager.requestAlwaysAuthorization()
            print("Not authorized")
        }
    }
}
