//
//  Medals.swift
//  SportsTracker
//
//  Created by leevilim on 5.5.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation

/**
 Defines the requirements for medal classes of the application.
 
 Medals are used as to display different achievements to the user on a per completed activity basis.
 During activities, user can earn medals, which are linked to activities by the activity ID.
 These medals are saved to CoreData and can be later fetched with the activity ID.
 */
protocol MedalObject {
    
    /**
     Identifier for the medal, displayed to the user
     */
    var identifier: String { get }
    
    /**
     Id for the medal itself
     */
    var id: UUID { get }
    
    /**
     ID of the activity which this medal is linked to
     */
    var activityId: UUID { get }
    
    /**
     Image file used to display this medal in UI.
     */
    var imageFile: String { get }
    
    /**
     Desciption displayed to the user about the medal
     */
    var description: String { get }

}

/**
 Implementations of Different Medal classes below.
 All the medals need to get the activity id when initialized.
 */

/**
 */
class BronzeMedal: MedalObject {
    
    let id = UUID()
    
    let identifier: String = "Bronze Medal"
    
    let activityId: UUID
    
    let imageFile: String = "medalBronze"
    
    let description: String = "Activity distance 2-4 km"
    
    init(activityId: UUID) {
        self.activityId = activityId
    }
}

class SilverMedal: MedalObject {
    
    let id = UUID()

    let identifier: String = "Silver Medal"
    
    let activityId: UUID
    
    let imageFile: String = "medalSilver"
    
    let description: String = "Activity distance 4-7 km"
    
    init(activityId: UUID) {
        self.activityId = activityId
    }
}

class GoldMedal: MedalObject {
    
    let id = UUID()

    let identifier: String = "Gold Medal"
    
    let activityId: UUID
    
    let imageFile: String = "medalGold"
    
    let description: String = "Activity distance 7-10 km"
    
    init(activityId: UUID) {
        self.activityId = activityId
    }
}

class PlatinumMedal: MedalObject {
    
    let id = UUID()

    let identifier: String = "Platinum Medal"
    
    let activityId: UUID
    
    let imageFile: String = "medalPlatinum"
    
    let description: String = "Activity distance 10+ km"
    
    init(activityId: UUID) {
        self.activityId = activityId
    }
}
