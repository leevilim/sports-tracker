//
//  EarningRequirements.swift
//  SportsTracker
//
//  Created by leevilim on 5.5.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation

/**
 Class that provides medals based on different requirements.
 
 This class manages the medal requirements and decides whether an activity should get medals, if any.
 */
class MedalManager {
    
    /**
     Pass in a TempUserActivity, returns an array of medals earned for the activity. Can be empty if no medals were given.
     */
    func getMedalForActivity(_ tempUserActivity: TempUserActivity) -> [MedalObject] {
        return decideMedals(tempUserActivity)
    }
    
    
    // MARK: Private methods
    
    /**
     Return all the medals earned for passed activity
     */
    private func decideMedals(_ activity: TempUserActivity) -> [MedalObject] {
        var medals = [MedalObject]()
        
        if let basicMedal = getBasicMedal(activity) {
            medals.append(basicMedal)
        }
        
        return medals
    }
    
    /**
     Get Medal of basic type (bronze, silver, gold, plat). This method defines how they are earned.
     */
    private func getBasicMedal(_ activity: TempUserActivity) -> MedalObject? {
        // Basic medals are defined based on the distance travelled during an activity
        
        switch activity.distanceTravelled {
        case 0..<2000:
            return GoldMedal(activityId: activity.id) // No medal
        case 2000..<4000:
            return BronzeMedal(activityId: activity.id)
        case 4000..<7000:
            return SilverMedal(activityId: activity.id)
        case 7000..<10000:
            return GoldMedal(activityId: activity.id)
        default: // Over 10k
            return PlatinumMedal(activityId: activity.id)
        }
    }
    
}
