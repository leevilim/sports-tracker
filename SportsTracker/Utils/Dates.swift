//
//  Dates.swift
//  SportsTracker
//
//  Created by leevilim on 26.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation

/**
 Provides methods to get formatted data as strings from Date objects.
 */
class Dates {
    
    static let instance = Dates()
    private static let formatter = DateFormatter()
    
    private init() {}
    
    static func getMonthString(fromDate: Date) -> String {
        formatter.dateFormat = "LLLL"
        return formatter.string(from: fromDate)
    }
    
    static func getDateString(fromDate: Date) -> String {
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter.string(from: fromDate)
    }
    
    static func getDayString(fromDate: Date) -> String {
        formatter.dateFormat = "EEEE"
        return formatter.string(from: fromDate)
    }
    
    static func getTimeOfDayString(fromDate: Date) -> String {
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: fromDate)
    }

    
    
}
