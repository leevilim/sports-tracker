//
//  Stopwatch.swift
//  SportsTracker
//
//  Created by leevilim on 19.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation

/**
 Stopwatch, which tracks elapsed time with the defined accuracy.
 */
class Stopwatch {
    
    // How accurate we want the stopwatch be, second is fine for our application
    private let timeInterval: Double = 1.0
    
    var delegate: StopWatchDelegate?
    
    // Value for this timer, as seconds
    private (set) var timerValue: Double = 0
    {
        didSet {
            // Inform delegate of change on every update
            delegate?.timerChanged(timerValue: self.timerValue)
        }
    }
    
    private var timer: Timer?
    
    init() {}

    func start(){
        // Just to make sure we only have one timer running at a time
        timer?.invalidate()
        
        self.timer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true) { timer in
            self.timerValue += self.timeInterval
        }
    }
    
    
    func stop(){
        timer?.invalidate()
    }
    
}

/**
 Implement in class to receiver updates from a StopWatch whenever its timerValue changes.
 */
protocol StopWatchDelegate {
    func timerChanged(timerValue: Double)
}
