//
//  Calculator.swift
//  SportsTracker
//
//  Created by leevilim on 21.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.

import Foundation

/**
 Calculate different things for the application needs
 */
class Calculator {
    
    static let instance = Calculator()
    
    private init() {}
    
    
    static func getAsKilometersPerHour(fromMetersPerSecond: Double) -> Double {
        return fromMetersPerSecond * 3.6
    }
    
    static func getAsKilometers(fromMeters: Double) -> Double {
        return fromMeters/1000
    }   
    
    
    /**
     Calculate an estimate for burned calories during an activity.
     
     Total calories burned = Duration (in minutes)*(MET*3.5*weight in kg)/200
     Formula from: https://www.verywellfit.com/how-many-calories-you-burn-during-exercise-4111064
     MET values from: https://golf.procon.org/met-values-for-800-activities/
     */
    static func burnedCalories(activity: TempUserActivity, weight: Double) -> Double {
        let part1 = activity.totaltime ?? 0
        guard let speed = activity.averageSpeed  else {
            return 0
        }
        let part2 = getMETEstimate(speed: speed) * 3.5 * weight
        return part1 * part2 / 200 / 1000
    }
    
    // Get MET estimate for a speed value (km/h)
    private static func getMETEstimate(speed: Double) -> Double {
        switch speed {
        case nil:
            return 0
        case 0...6.4:
            return 6.0
        case 6.4...8.0:
            return 8.3
        case 8.0...9.65:
            return 9.8
        case 9.65...11.2:
            return 11.0
        case 11.2...12.9:
            return 11.8
        case 12.9...14.5:
            return 12.8
        case 14.5...16.1:
            return 14.5
        case 16.1...17.7:
            return 16.0
        case 17.7...19.3:
            return 19.0
        case 19.3...20.9:
            return 19.8
        default:
            return 23.0
        }
    }
}
