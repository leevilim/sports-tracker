//
//  File.swift
//  SportsTracker
//
//  Created by leevilim on 19.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation
import CoreLocation

/**
 Handles the dependency injection for the application
 */
class Injector {
    
    static let instance = Injector()
    
    private init() {}
    
    /**
     Returns a new instance of an ActivityTracker.
     Provide a class which wants to listen for updates from the initialized ActivityTracker
     */
    static func provideActivityTracker(updateListener: ActivityTrackerUpdateListener) -> ActivityTracker {
        return ActivityTracker(manager: CLLocationManager(), updateListener: updateListener)
    }
    
    /**
     Returns a new instance of an ActivityManager. When this method is called, register a Date for UserActivity, which indicates when the activity has started.
     */
    static func provideActivityManager() -> ActivityManager {
        return ActivityManager(
            userActivity: TempUserActivity(),
            stopWatch: Stopwatch(),
            coreDataManager: provideUserActivityCDM(),
            defaultManager: provideUserDefaultManager(),
            medalManager: MedalManager()
        )
    }
    
    /**
     Returns the shared instance of UserActivityCoreDataManager
     */
    static func provideUserActivityCDM() -> UserActivityCDM {
        return UserActivityCDM.sharedInstance as! UserActivityCDM
    }
    
    /**
     Returns the shared intance of MedalCoreDataManager
     */
    static func provideMedalCDM() -> MedalCDM {
        return MedalCDM.sharedInstance as! MedalCDM
    }
    
    
    static func provideUserDefaultManager() -> UserDefaultManager {
        return UserDefaultManager.sharedInstance
    }
        
}
