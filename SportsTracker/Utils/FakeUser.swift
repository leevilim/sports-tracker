//
//  FakeUser.swift
//  SportsTracker
//
//  Created by leevilim on 26.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation

/**
 Fake user stats until real ones implemented weight in kg
 */
class FakeUser {
    
    let userName: String = "username"
    let weight: Double = 88
}
