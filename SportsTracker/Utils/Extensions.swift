//
//  Extensions.swift
//  SportsTracker
//
//  Created by leevilim on 26.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import Foundation
import UIKit
import MapKit

/**
 Add some methods to UIViewControllers
 */
extension UIViewController {
    
    /**
     Creates a back button for the ViewController calling this method.
     
     Dismisses the current VC when clicking.
     */
    func createBackButton () {
        
        let backbutton = UIButton(type: .custom)
                backbutton.setImage(UIImage(named: "BackButton.png"), for: .normal)
                backbutton.setTitle("Back", for: .normal)
                backbutton.setTitleColor(backbutton.tintColor, for: .normal)
        backbutton.addTarget(self, action: #selector(self.backAction), for: .touchUpInside)
        
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backbutton)
    }

    @objc func backAction() -> Void {
        dismiss(animated: true, completion: nil)
    }
}


extension Int {
    /**
    Easy way to get formatted time from seconds
    */
    func secondsToTime() -> String {

        let (h,m,s) = (self / 3600, (self % 3600) / 60, (self % 3600) % 60)

        // Hours only need to be displayed if not 0
        var h_string = ""
        
        if h != 0 {
            h_string = h < 10 ? "0\(h):" : "\(h):"
        }
        
        let m_string =  m < 10 ? "0\(m)" : "\(m)"
        let s_string =  s < 10 ? "0\(s)" : "\(s)"

        return "\(h_string)\(m_string):\(s_string)"
    }
}

extension CLLocation {
    
    /**
     Compare the timestamp of this location and a given date. Returns boolean value based on whether the Location should be ignored
     or saved.
     */
    func shouldLocationBeIgnored(anotherDate d: Date) -> Bool {
        let timeSelf = Int(self.timestamp.timeIntervalSince1970)
        let timeDate = Int(d.timeIntervalSince1970)
        return timeSelf-timeDate <= 1
    }

}

extension UIView {
    
    // Used for UITableViewHeader
    func bindEdgesToSuperview() {
        
        guard let s = superview else {
            preconditionFailure("`superview` nil in bindEdgesToSuperview")
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        leadingAnchor.constraint(equalTo: s.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: s.trailingAnchor).isActive = true
        topAnchor.constraint(equalTo: s.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: s.bottomAnchor).isActive = true
    }
}

// Add border color to view
extension UIView {
    public func addViewBorder(borderColor:CGColor,borderWith:CGFloat,borderCornerRadius:CGFloat){
        self.layer.borderWidth = borderWith
        self.layer.borderColor = borderColor
        self.layer.cornerRadius = borderCornerRadius

    }
}

// Add padding to textfields
extension UITextField {

enum PaddingSpace {
    case left(CGFloat)
    case right(CGFloat)
    case equalSpacing(CGFloat)
}

func addPadding(padding: PaddingSpace) {

    self.leftViewMode = .always
    self.layer.masksToBounds = true

    switch padding {

    case .left(let spacing):
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
        self.leftView = leftPaddingView
        self.leftViewMode = .always

    case .right(let spacing):
        let rightPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
        self.rightView = rightPaddingView
        self.rightViewMode = .always

    case .equalSpacing(let spacing):
        let equalPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
        // left
        self.leftView = equalPaddingView
        self.leftViewMode = .always
        // right
        self.rightView = equalPaddingView
        self.rightViewMode = .always
    }
}
}

extension String {
    public var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}

// Toast messages
extension UIViewController{
func showToast(message : String, seconds: Double){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = .white
        alert.view.alpha = 0.5
        alert.view.layer.cornerRadius = 15
        self.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }
 }

// Sort dictionary by keys
extension Dictionary where Key: Comparable {
    var sortedByKey: [(Key, Value)] { return Array(self).sorted { $0.0 < $1.0 } }
}

extension Dictionary {
  func contains(key: Key) -> Bool {
    self.index(forKey: key) != nil
  }
}

// Get a day compared to current date 
extension Date {
    mutating func changeDays(by days: Int) {
        self = Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
}
