//
//  HomeViewController.swift
//  SportsTracker
//
//  Created by leevilim on 26.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    private let defaults = Injector.provideUserDefaultManager()
    
    // PlaceHolder imageviews for medals in ui since the scaling is a nightmare to setup with runtime generated elements
    @IBOutlet weak var imageMedal1: UIImageView!
    @IBOutlet weak var imageMedal2: UIImageView!
    @IBOutlet weak var imageMedal3: UIImageView!
    
    // Display if no earned medals
    @IBOutlet weak var labelGuide: UILabel!
    
    @IBOutlet weak var helloLabel: UILabel!
    
    let medals = Injector.provideMedalCDM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        loadMostRecentMedals()
    }
    
    private func setUI(){
        helloLabel.text = "Hello, \(defaults.getUserName())!"
    }
    
    
    // Ugly way to show medals in UI
    private func loadMostRecentMedals(){
        let recentMedals = medals.getMostRecentMedals(count: 3)
        
        if recentMedals.count == 0 {
            labelGuide.text = "Complete activities to earn medals"
            return
        }
        
        recentMedals.forEach { medal in
            let image = UIImage(named: medal.imageFile!)
            
            switch recentMedals.firstIndex(of: medal) {
            case 0:
                imageMedal1.image = image
            case 1:
                imageMedal2.image = image
            case 2:
                imageMedal3.image = image
            default:
                print("Something is wrong")
            }
            
        }
        
    }
}
