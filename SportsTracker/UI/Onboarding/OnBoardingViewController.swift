//
//  OnBoardingViewController.swift
//  SportsTracker
//
//  Created by leevilim on 4.5.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import UIKit


/**
 Provides a view for the user to edit personal information for the application.
 
 Also works as a first time entry screen for the application.
 */
class OnBoardingViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textViewUsername: UITextField!
    @IBOutlet weak var textViewWeight: UITextField!
    
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var labelInformation: UILabel!
    private let defaults = Injector.provideUserDefaultManager()
    
    // Alter ui text fields if the view was opened via settings
    var openedFromSettings = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        changeLabels()
        // Do any additional setup after loading the view.
        self.textViewUsername.delegate = self;
        self.textViewWeight.delegate = self;

    }
    
    // Style the UI elements / add data if the screen is used to edit existing
    private func setUI(){
        textViewWeight.addViewBorder(borderColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), borderWith: 1.0, borderCornerRadius: 6)
        textViewUsername.addViewBorder(borderColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), borderWith: 1.0, borderCornerRadius: 6)

        textViewUsername.addPadding(padding: .equalSpacing(10))
        textViewWeight.addPadding(padding: .equalSpacing(10))
        
        let name = defaults.getUserName()
        let weight = defaults.getUserWeight()
        
        if weight != 0 {
            textViewWeight.text = "\(Int(weight))"
        }
        textViewUsername.text = name
    }
    
    private func changeLabels(){
        if openedFromSettings {
            labelHeader.text = "Edit information"
            labelInformation.text = "Change the fields below"
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.textViewUsername.endEditing(true)
        self.textViewWeight.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       textField.resignFirstResponder()
       return true
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        // Validate user inputs
        let usernameFails = textViewUsername.text?.isEmpty ?? false
        let weightFails = !(textViewWeight.text?.isNumber ?? false)
        
        if usernameFails {
            self.showToast(message: "Username cant be empty", seconds: 1.0)
            return false
        }
        
        if weightFails {
            self.showToast(message: "Weight cant be empty and must only contain numbers", seconds: 1.5)
            return false
        }
        
        // Save validated data to UserDefaults
        defaults.setUserName(string: textViewUsername.text!)
        let string = String(textViewWeight.text!) as NSString
        defaults.setUserWeight(weight: string.doubleValue)
        
        return true
    }
}

