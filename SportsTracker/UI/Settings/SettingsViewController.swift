//
//  SettingsViewController.swift
//  SportsTracker
//
//  Created by leevilim on 28.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    private let defaults = Injector.provideUserDefaultManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = defaults.getUserName()
        weightLabel.text = "\(defaults.getUserWeight()) kg"
        
    }

    // Navigate to onboarding vc to change user data
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SettingsToOnBoarding" {
            let vc = segue.destination as! OnBoardingViewController
            vc.openedFromSettings = true
        }
    }
}
