//
//  ActivitySingleViewController.swift
//  SportsTracker
//
//  Created by leevilim on 22.4.2021.
//  Copyright © 2021 leevlim. All rights reserved.
//

import UIKit
import MapKit

/**
 ViewController for single activity screen
 */
class ActivitySingleViewController: UIViewController, MKMapViewDelegate {

    /**
     UserActivity linked to this view.
     
     Used to display the data.
     */
    var activity: UserActivity?

    
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var labelDay: UILabel!
    
    @IBOutlet weak var labelTime: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    
    @IBOutlet weak var labelActivityTime: UILabel!
    
    @IBOutlet weak var labelActivityDistance: UILabel!
    
    @IBOutlet weak var labelActivitySpeed: UILabel!
    
    @IBOutlet weak var labelActivityCalories: UILabel!
    
    @IBOutlet weak var imageMedal: UIImageView!
    
    /**
     Fetch the medals which were given to this activity
     */
    private var medals: [Medal] = [Medal]()
    
    private let medalCDM = Injector.provideMedalCDM()
    
    private let activityCDM = Injector.provideUserActivityCDM()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        loadMedals()
        self.mapView.delegate = self
        self.mapView.mapType = .mutedStandard
        
        DispatchQueue.main.async {
            self.drawPolylines()
            self.setPinsToStartStop()
        }
    }
    
    private func loadMedals(){
        guard let act = self.activity else {
            print("Activity was nil")
            return
        }
        self.medals.append(contentsOf: medalCDM.getMedalsForActivity(act))
        
        // Currently an activity can have only a single medal
        if let medal = medals.first {
            print("Medal should be visible now \(medal.imageFile!)")
            imageMedal.image = getMedalImage(image: medal.imageFile!)
        }
    }
    
    private func getMedalImage(image: String) -> UIImage {
        switch image {
        case "medalBronze":
            return UIImage(named: "medalBronze")!
        case "medalSilver":
            return UIImage(named: "medalSilver")!
        case "medalPlatinum":
            return UIImage(named: "medalPlatinum")!
        case "medalGold":
            return UIImage(named: "medalGold")!
        default:
            print("NoImage")
        }
        return UIImage()
    }
    
    private func setPinsToStartStop(){
        let coords = activity?.locationGeopoints as? [CLLocation]
        let start = coords?.first
        let stop = coords?.last
        
        print("these values\(start) \(stop)")
        
        if start != nil && stop != nil {
            let sMarker = MKPointAnnotation()
            sMarker.coordinate = start?.coordinate as! CLLocationCoordinate2D
            sMarker.title = "Start"
            
            let stopMarker = MKPointAnnotation()
            stopMarker.coordinate = stop?.coordinate as! CLLocationCoordinate2D
            stopMarker.title = "Finish"
            
            mapView.addAnnotations([sMarker, stopMarker])
        }
    }

    private func setUI(){
        
        guard let activityDate = activity?.timeStamp else {
            print("Date should not be nil ever, somehow it is here")
            return
        }
        
        let date = Dates.getDateString(fromDate: activityDate)
        let weekDay = Dates.getDayString(fromDate: activityDate)
        let timeOfDay = Dates.getTimeOfDayString(fromDate: activityDate)
        
        labelDate.text = date
        labelDay.text = weekDay
        labelTime.text = timeOfDay
        
        let km = Calculator.getAsKilometers(fromMeters: activity?.distanceTravelled ?? 0)
        let kmh = activity?.averageSpeed ?? 0
        let time = Int(activity?.totaltime ?? 0).secondsToTime()
        
        labelActivityTime.text = time
        labelActivitySpeed.text = "\(String(format:"%0.1f km/h", kmh))"
        labelActivityDistance.text = "\(String(format:"%0.2f km", km))"
        labelActivityCalories.text = String(format: "%0.0f kcal", activity?.caloriesBurned ?? 0)
    }
    
    private func drawPolylines(){
        // Get the CLLocations
        guard let clLocations = activity?.locationGeopoints as? [CLLocation] else {
            print("Cant draw polylines")
            return
        }
        let coords = clLocations.map {
            $0.coordinate
        }
        
        let polyline = MKPolyline(
            coordinates: coords ,
            count: coords.count
        )
        mapView.addOverlay(polyline)
        setVisibleMapArea(polyline: polyline, edgeInsets: UIEdgeInsets(top: 40.0, left: 20.0, bottom: 40.0, right: 20.0))
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let routePolyline = overlay as? MKPolyline {
            let renderer = MKPolylineRenderer(polyline: routePolyline)
            renderer.strokeColor = UIColor.blue.withAlphaComponent(0.9)
            renderer.lineWidth = 7
            return renderer
        }

        return MKOverlayRenderer()
    }
    
    private func setVisibleMapArea(polyline: MKPolyline, edgeInsets: UIEdgeInsets, animated: Bool = false) {
        mapView.setVisibleMapRect(polyline.boundingMapRect, edgePadding: edgeInsets, animated: animated)
    }

    // Customize annotations
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "MyMarker")
        
        // Change the look of start and finish annotations
        switch annotation.title {
        case "Start":
            annotationView.markerTintColor = UIColor.init(red: 0, green: 0.5, blue: 0, alpha: 1)
            annotationView.glyphImage = UIImage(named: "start")
        case "Finish":
            annotationView.markerTintColor = UIColor.orange
            annotationView.glyphImage = UIImage(named: "stop")
        default:
            print("Keep default annotation")
        }

        return annotationView
    }
    
    
    
    @IBAction func deleteActivity(_ sender: Any) {
        activityCDM.deleteActivity(self.activity!)
        performSegue(withIdentifier: "backToHistory", sender: self)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "backToHistory" {
            let navController = segue.destination as! UINavigationController
            let vc = navController.topViewController as! ActivityHistoryTableViewController
            vc.tableView.reloadData()
            vc.dataProvider = ActivityGroupedProvider(initialDataSet: activityCDM.requestAllUserActivities())
        }
    }
}
