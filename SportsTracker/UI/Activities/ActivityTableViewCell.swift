//
//  ActivityTableViewCell.swift
//  SportsTracker
//
//  Created by leevilim on 26.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import UIKit

/**
 TableViewCell for UserActivities
 */
class ActivityTableViewCell: UITableViewCell {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDistance: UILabel!

    @IBOutlet weak var imageRoute: UIImageView!
    
    @IBOutlet weak var imageMedal: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBInspectable var cornerRadius: CGFloat {
           get {
               return layer.cornerRadius
           }
           set {
               layer.cornerRadius = newValue
               layer.shadowRadius = newValue
               layer.masksToBounds = false
           }
       }

       @IBInspectable var shadowOpacity: Float {
           get {
               return layer.shadowOpacity
           }
           set {
               layer.shadowOpacity = newValue/100
               layer.shadowColor = UIColor.darkGray.cgColor
           }
       }

       @IBInspectable var shadowOffset: CGSize {
           get {
               return layer.shadowOffset
           }
           set {
               layer.shadowOffset = newValue
               layer.shadowColor = UIColor.black.cgColor
               layer.masksToBounds = false
           }
       }
}
