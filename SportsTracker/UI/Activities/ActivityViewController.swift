//
//  ActivityViewController.swift
//  SportsTracker
//
//  Created by leevilim on 21.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import UIKit

/**
 ViewController for the screen when the user is performing an activity.
 */
class ActivityViewController: UIViewController, ActivityManagerUpdateListener {

    /**
     Initialize an ActivityTracker, and an ActivityManager.
     ActivityTracker is the one to be interacted with, and it forwards the information to ActivityManager.
     */
    private var activityManager: ActivityManager?
    private var activityTracker: ActivityTracker?
    private let cdm = Injector.provideUserActivityCDM()
    
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelSpeed: UILabel!
    
    private var setOnPause = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityManager = Injector.provideActivityManager()
        activityTracker = Injector.provideActivityTracker(updateListener: activityManager!)
        
        activityManager!.delegate = self
        activityTracker!.startActivity()
    }
    
    
    // MARK: ActivityManagerUpdateListener
    
    func onTimeChanged(_ timeValue: Double) {
        labelTime.text = "\(Int(timeValue).secondsToTime())"
    }
    
    func onStatusUpdate(currentDistance: Double, currentSpeed: Double) {
        labelDistance.text =  String(format: "%0.2f km", currentDistance)
        labelSpeed.text = String(format: "%0.1f km/h", currentSpeed)
    }
    
    @IBAction func buttonPause(_ sender: Any) {
        setOnPause = !setOnPause
        
        if(setOnPause) {
            labelStatus.text = "Activity paused"
            activityTracker?.pauseActivity()
            return
        }
        
        labelStatus.text = "Activity runnning"
        activityTracker?.startActivity()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "trackerToSingle") {
            activityTracker!.finishActivity()
            let navController = segue.destination as! UINavigationController
            let vc = navController.topViewController as! ActivitySingleViewController
            vc.activity = cdm.requestLatestUserActivity()
        }
    }
}
