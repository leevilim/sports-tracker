//
//  ActivityHistoryTableViewController.swift
//  SportsTracker
//
//  Created by leevilim on 26.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import UIKit
import MapKit

/**
 ViewController providing the history view for the completed UserActivities.
 */
class ActivityHistoryTableViewController: UITableViewController {
    
    // Save loaded route images to cache
    private var routeCache = [UserActivity: UIImage]()

    // Get reference to UserActivityCoreDataManager
    private let cdm = Injector.provideUserActivityCDM()
    
    private let medals = Injector.provideMedalCDM()
    
    // Data in grouped form
    var dataProvider: ActivityGroupedProvider!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Initialize the dataset with all the user data
        dataProvider = ActivityGroupedProvider(initialDataSet: cdm.requestAllUserActivities())
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return dataProvider.getGroupCount()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProvider.getItemCountForGroup(section)
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "activityHistoryCell", for: indexPath)
            as? ActivityTableViewCell else {
            fatalError("Cell should not be nil here")
        }
        
        let data = dataProvider.getItemDataAtGroupIndex(
            groupIndex: indexPath.section,
            itemIndex: indexPath.row
        )
        
        let distance = Calculator.getAsKilometers(fromMeters: data.distanceTravelled)
        let time = Int(data.totaltime)
        let medalArr = medals.getMedalsForActivity(data)

        // Add a image if medal exists for the activity
        if medalArr.count != 0 {
            cell.imageMedal.image = UIImage(named: medalArr[0].imageFile!)
        }
        
        cell.labelDate.text = Dates.getDateString(fromDate: data.timeStamp!)
        cell.labelTime.text = time.secondsToTime()
        cell.labelDistance.text = String(format: "%0.2f km", distance)
        
        DispatchQueue.main.async {
            self.loadMapPolyline(userActivity: data, imageView: cell.imageRoute)
        }
        
        return cell
    }

 
    // Header for section
    override func tableView(_ tableView: UITableView,
                           viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(
        frame: CGRect(x: 40, y: 0, width: tableView.bounds.width - (20 * 2), height: 120))

        let label = UILabel()
        view.addSubview(label)
        
        label.backgroundColor = .white
        label.font = UIFont.italicSystemFont(ofSize: 25)
        label.textColor = .black
        label.bindEdgesToSuperview()
        

        // TODO Fix margin and remove spaces
        label.text = "   " + dataProvider.getGroupAtIndex(section)
        view.bringSubviewToFront(label)
        return view
    }
    
    override func tableView(_ tableView: UITableView,
                       heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(60.0)
    }

    // Item size
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
        
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Set the activity data for ActivitySingleViewController
        if segue.identifier == "historyToSingle" {
            let navController = segue.destination as? UINavigationController
            let vc = navController?.topViewController as! ActivitySingleViewController
            
            guard let section = tableView.indexPathForSelectedRow?.section else {
                return
            }
            guard let item = tableView.indexPathForSelectedRow?.item else {
                return
            }
            let data = dataProvider.getItemDataAtGroupIndex(groupIndex: section, itemIndex: item)

            vc.activity = data
        }

    }

    
    /**
     Code below modified from: https://stackoverflow.com/a/42107149/14839410
     
     TODO Use the function when finishing an entry and save the image data to CoreData and link it to an activity.
     Would be alot better than to draw the lines every time this class is initialized.
     */
    
    
    /**
     Load an image of the polyline for the activity
     */
    private func loadMapPolyline(userActivity: UserActivity, imageView: UIImageView) {
        let mapSnapShotOptions = MKMapSnapshotter.Options()
        
        guard let clLocations = userActivity.locationGeopoints as? [CLLocation] else {
            print("Cant draw polylines for this activity")
            return
        }
        
        var coords = [CLLocationCoordinate2D]()
        
        clLocations.forEach {
            coords.append( $0.coordinate)
        }
        
        // Dont try to draw line if only 2 locations
        if coords.count <= 2 {
            return
        }
        
        // Set the region of the map that is rendered. (by polyline)
        let polyline = MKPolyline(coordinates: coords, count: coords.count)
        let region = MKCoordinateRegion(polyline.boundingMapRect)

        mapSnapShotOptions.region = region
    
    
        mapSnapShotOptions.scale = UIScreen.main.scale
        mapSnapShotOptions.size = CGSize.init(width: 160, height: 134)

        mapSnapShotOptions.showsBuildings = false
        

        let snapShotter = MKMapSnapshotter(options: mapSnapShotOptions)

        snapShotter.start() { snapshot, error in
            guard let snapshot = snapshot else {
                return
            }
            
            // Use cached image if it exists
            if self.routeCache.contains(key: userActivity) {
                imageView.image = self.routeCache[userActivity]
            }
            // If not cached, draw it and save
            self.routeCache.updateValue(self.drawLineOnImage(snapshot: snapshot, coords: coords), forKey: userActivity)
            imageView.image = self.routeCache[userActivity]
        }
    }

    
    func drawLineOnImage(snapshot: MKMapSnapshotter.Snapshot ,coords: [CLLocationCoordinate2D]) -> UIImage {
        let image = snapshot.image

        // draw original image into the context
        image.draw(at: CGPoint.zero)
        
        // Init a context
        UIGraphicsBeginImageContext(CGSize.init(width: 120, height: 120))

        // Get the context for CoreGraphics
        let context = UIGraphicsGetCurrentContext()

        // set stroking width and color of the context
        context!.setLineWidth(2.0)
        context!.setStrokeColor(UIColor.blue.cgColor)

        // Here is the trick :
        // We use addLine() and move() to draw the line, this should be easy to understand.
        // The diificult part is that they both take CGPoint as parameters, and it would be way too complex for us to calculate by ourselves
        // Thus we use snapshot.point() to save the pain.
        context!.move(to: snapshot.point(for: coords[0]))
        for i in 0...coords.count-1 {
          context!.addLine(to: snapshot.point(for: coords[i]))
          context!.move(to: snapshot.point(for: coords[i]))
        }

        // apply the stroke to the context
        context!.strokePath()

        // get the image from the graphics context
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()

        // end the graphics context
        UIGraphicsEndImageContext()

        return resultImage!
    }

}


