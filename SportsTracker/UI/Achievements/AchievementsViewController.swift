//
//  AchievementsViewController.swift
//  SportsTracker
//
//  Created by iosdev on 6.5.2021.
//  Copyright © 2021 iosdev. All rights reserved.
//

import UIKit

class AchievementsViewController: UIViewController {

    @IBOutlet weak var BronzeLabel: UILabel!
    @IBOutlet weak var SilverLabel: UILabel!
    @IBOutlet weak var GoldLabel: UILabel!
    @IBOutlet weak var MasterLabel: UILabel!
    
    @IBOutlet weak var thisWeekBronze: UILabel!
    @IBOutlet weak var thisWeekSilver: UILabel!
    @IBOutlet weak var thisWeekGold: UILabel!
    @IBOutlet weak var thisWeekPlat: UILabel!
    
    
    @IBOutlet weak var medalTotalCount: UILabel!
    @IBOutlet weak var medalThisWeekCount: UILabel!
    
    private let medals = Injector.provideMedalCDM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        self.createBackButton()
    }
    

    private func setUI(){
        let bronze = medals.getMedalsViaIdentifier(string: "Bronze Medal").count
        let silver = medals.getMedalsViaIdentifier(string: "Silver Medal").count
        let gold = medals.getMedalsViaIdentifier(string: "Gold Medal").count
        let plat = medals.getMedalsViaIdentifier(string: "Platinum Medal").count
        
        let totalMedals = bronze + silver + gold + plat
        
        BronzeLabel.text = "x \(bronze)"
        SilverLabel.text = "x \(silver)"
        GoldLabel.text = "x \(gold)"
        MasterLabel.text = "x \(plat)"
        medalTotalCount.text = "x \(totalMedals)"
        
        // Medal counts for week
        
        var dateWeekAgo = Date()
        dateWeekAgo.changeDays(by: -6)
        
        let weekB = medals.getMedalsUpFromDate(date: dateWeekAgo, identifier: "Bronze Medal").count
        let weekS = medals.getMedalsUpFromDate(date: dateWeekAgo, identifier: "Silver Medal").count
        let weekG = medals.getMedalsUpFromDate(date: dateWeekAgo, identifier: "Gold Medal").count
        let weekP = medals.getMedalsUpFromDate(date: dateWeekAgo, identifier: "Platinum Medal").count
            
        let weekTotal = weekB + weekS + weekG + weekP
            
        thisWeekBronze.text = "x \(weekB)"
        thisWeekSilver.text = "x \(weekS)"
        thisWeekGold.text = "x \(weekG)"
        thisWeekPlat.text = " x \(weekP)"
        medalThisWeekCount.text = "x \(weekTotal)"
    }

}
