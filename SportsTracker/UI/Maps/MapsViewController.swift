//
//  MapsViewController.swift
//  SportsTracker
//
//  Created by leevilim on 19.4.2021.
//  Copyright © 2021 leevilim. All rights reserved.
//

import UIKit
import MapKit

/**
 ViewController which contents are visible in the tab navigation at map tab.
 
 Implements CLLMDelegate to receive initial location of the user / ask permissions
 */
class MapsViewController: UIViewController {

    private let locationManager = CLLocationManager()
    
    @IBOutlet weak var buttonStart: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.requestLocation()
    }
    
    @IBAction func buttonStartActivity(_ sender: Any) {
        // Navigates to ActivityVC
    }
        
    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any?) -> Bool {
        
        // If no permission to location, prompt user to enable
        if !hasLocationPermission() {
            let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable always location permissions in settings.", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                //Redirect to Settings app
                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            alertController.addAction(cancelAction)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            return false
        }

        return true
    }
    
    // Check if there is always on permissions
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways:
                hasPermission = true
            default:
                hasPermission = false
            }
            
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }
}

extension MapsViewController: CLLocationManagerDelegate {
    
    // MARK: LocationManager Delegate
    
    /**
     Handle the location updates here
     */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let myCoord = locations.last else {
            print("Location was null")
            return
        }
        
        // Create a marker
        let userLocation = MKPointAnnotation()
        userLocation.title = "You"
        userLocation.subtitle = "Your location"
        userLocation.coordinate = myCoord.coordinate
        mapView.addAnnotation(userLocation)
        
        mapView.centerToLocation(myCoord)
        self.locationManager.stopUpdatingLocation()
    }
    
    /**
     Any errors with the location service comes here
     */
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       if let error = error as? CLError, error.code == .denied {
          // Location updates are not authorized.
        print("Authorization error in ActivityTracker")
          manager.stopUpdatingLocation()
          locationManager.requestAlwaysAuthorization()
          return
       }
        print("Other error in ActivityTracker : \(error)")
       // Notify the user of any errors.
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      guard status == .authorizedAlways else {
        print("not authorized")
        manager.requestAlwaysAuthorization()
        return
      }
      print("Here")
      locationManager.startUpdatingLocation()
    }

    
}

private extension MKMapView {
  func centerToLocation(
    _ location: CLLocation,
    regionRadius: CLLocationDistance = 1000
  ) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}
